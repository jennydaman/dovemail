FROM docker.io/lukemathwalker/cargo-chef:0.1.66-rust-1.78-alpine3.19 AS chef
WORKDIR /app
ARG CARGO_TERM_COLOR=always

FROM chef AS planner
COPY . .
RUN cargo chef prepare --recipe-path recipe.json

FROM chef AS builder
COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --release --locked --target $(uname -m)-unknown-linux-musl --recipe-path recipe.json
COPY . .
RUN cargo build --release --locked --target $(uname -m)-unknown-linux-musl
RUN mkdir /app/dist
RUN cp /app/target/$(uname -m)-unknown-linux-musl/release/dovemail /app/dist/dovemail

FROM scratch
COPY --from=builder /app/dist/dovemail /app/dovemail
ENV PATH=/app

LABEL org.opencontainers.image.authors="Jennings Zhang <dev@sl.jennin.xyz>" \
    org.opencontainers.image.url="https://gitlab.com/jennydaman/dovemail" \
    org.opencontainers.image.licenses="Hippocratic License 3.0" \
    org.opencontainers.image.title="dovemail" \
    org.opencontainers.image.description="Parse the output of m365 to maildir plaintext files."

CMD ["/app/dovemail"]
