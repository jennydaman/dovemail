# Dovemail

```
                    _                                _ _ 
                   | |                              (_) |
  <(' ) /"""/    __| | _____   _____ _ __ ___   __ _ _| |
    \  /   /    / _` |/ _ \ \ / / _ \ '_ ` _ \ / _` | | |
     \.___"    | (_| | (_) \ V /  __/ | | | | | (_| | | |
                \__,_|\___/ \_/ \___|_| |_| |_|\__,_|_|_|

                   Check your email, peacefully.
```

`dovemail` is a simple\* utility for bridging Microsoft's official CLI tool,
[`m365`](https://aka.ms/cli-m365), with [Maildir](https://en.wikipedia.org/wiki/Maildir)
email clients.
It enables you to read Outlook messages using clients such as [Aerc](https://aerc-mail.org/),
[Neomutt](https://neomutt.org/), [Mozilla Thunderbird](https://www.thunderbird.net), or
[GNOME Evolution](https://gitlab.gnome.org/GNOME/evolution/-/wikis/home),
in situations where third-party applications are not allowed.
See below: [who is this for?](#who-is-this-for)

Retrieving mail works. Sending mail is WIP.

## Installation

### Install from crates.io

```shell
cargo install dovemail
```

### Docker Pull

```shell
docker pull registry.gitlab.com/jennydaman/dovemail/master:latest
```

### Podman Pull

```shell
podman pull registry.gitlab.com/jennydaman/dovemail/master:latest
```

### Installation Using Nix Flakes

Various methods.

#### Using Home-Manager

1. Add `inputs.dovemail.url = "gitlab:jennydaman/dovemail";` to `~/.home-manager/config/flake.nix`
2. Add `inputs.dovemail.packages.${system}.default` to `home.packages`
3. Run `home-manager switch`

#### Using `nix profile`

```shell
nix profile install gitlab:jennydaman/dovemail
```

## Quickstart

1. Run `m365 login`
2. Copy [`./scripts/pull_emails_using_dovemail.sh`](./scripts/pull_emails_using_dovemail.sh) and run it.

## Design

Dovemail's design is "\*simple" in the UNIX philosophy sense of "do one thing and one thing well."
This repository provides:

- `dovemail`, a "simple" Rust program
- `pull_emails_using_dovemail.sh`, a not-so-simple Bash script

### Usage: `dovemail`

`dovemail` parses JSON from stdin and writes plaintext files to an output directory.
Typical usage looks like:

```shell
m365 outlook message list | dovemail ~/.local/share/mail/bill_gates@example.com/AllMail/tmp
mv ~/.local/share/mail/bill_gates@example.com/AllMail/{tmp,new}
```

### Usage with Podman

```shell
podman run --rm \
  -v $HOME/.cli-m365-all-connections.json:/home/cli-microsoft365/.cli-m365-all-connections.json:ro \
  -v $HOME/.cli-m365-connection.json:/home/cli-microsoft365/.cli-m365-connection.json:ro \
  -v $HOME/.cli-m365-msal.json:/home/cli-microsoft365/.cli-m365-msal.json:ro \
  docker.io/m365pnp/cli-microsoft365:latest m365 outlook message list \
  | podman run --rm -i -v $HOME/.local/share/mail/bill_gates@example.com/AllMail/tmp:/out registry.gitlab.com/jennydaman/dovemail/master:latest dovemail /out
```

### Usage with Docker

Same as [Usage with Podman](#usage-with-podman), but replace `podman` with `docker`.

#### Implementation Details

The maildir [spec](https://cr.yp.to/proto/maildir.html) describes a procedure for producing
unique names, which `dovemail` does not implement. To my knowledge, this deviation from the
spec is benign. You can rename these files to conform to the spec, see the section on
[Configuration](#configuration).

`dovemail` names files after the [seahash](https://crates.io/crates/seahash) output on
the Outlook message ID.

### Usage: `pull_emails_using_dovemail.sh`

`pull_emails_using_dovemail.sh` creates a maildir-organized directory in `~/.local/share/mail`
and runs `m365 outlook message list | dovemail ...` for each folder to retrieve emails from Outlook.
On first run, all messages are downloaded. On subsequent run, only new messages are downloaded.

### Configuration

You're on your own. Create a copy `pull_emails_using_dovemail.sh` and change it however you'd like.

It'll be useful to create a cron/systemd timer/email client hotkey for running
(your copy of `pull_emails_using_dovemail.sh`).

#### My Example: Aerc

My own mail client of choice is [Aerc](https://aerc-mail.org/),
and I use [home-manager](https://nix-community.github.io/home-manager/)
to manage its configuration. My home-manager Nix code looks like:

```nix
{ lib, pkgs, ... }:
{
  accounts.email.accounts."bill_gates@example.com" = {
    primary = true;
    address = "bill_gates@example.com";
    realName = "Bill Gates";
    aerc.enable = true;
    aerc.extraAccounts = {
      source = "maildir://~/.local/share/mail/bill_gates@example.com";
      check-mail = "5m";
      check-mail-cmd = "~/.dotfiles/bin/pull_emails_using_dovemail.sh";
    };
  };
}
```

The generated `~/.config/aerc/accounts.conf` file looks like:

```
[bill_gates@example.com]
check-mail = 5m
check-mail-cmd = ~/.dotfiles/bin/pull_emails_using_dovemail.sh
copy-to = Sent
default = Inbox
from = Bill Gates <bill_gates@example.com>
postpone = Drafts
source = maildir://~/.local/share/mail/bill_gates@example.com
```

## Who is this for?

Most users hoping to connect mail clients with Outlook should _not_ use Dovemail.
One should try the following, in this order:

1. Connecting directly to Outlook IMAP/SMTP. https://support.microsoft.com/en-us/office/pop-imap-and-smtp-settings-for-outlook-com-d088b986-291d-42b8-9564-9c414e2aa040
2. Run the Davmail gateway. https://davmail.sourceforge.net/
3. Use `m365` CLI and **dovemail**.

Dovemail should only be used in situations where third-party applications are disabled by administrator.

### Is this allowed?

Use at your own risk!

In situations where third-party applications are being restricted by an administrator, would a tool like `dovemail` be allowed? Observe that third-party applications are blocked from using Outlook's API. Importantly, `dovemail` does _not_ use Outlook's API. It never connects with Microsoft's servers — it simply parses the output of `m365`, an official Microsoft tool. When using `dovemail`, only first-party software are used to communicate with Microsoft's servers.

Categorically, `dovemail` is more similar to accessibility software like screen-readers than a third-party client. It interfaces with first-party software (the `m365` CLI) and presents data to the user in a more accessible format (maildir, plaintext). If your organization allows disabled persons to use screen readers, there is no technical reason why `dovemail` should be disallowed.

## Limitations

- Mail attachments are not supported.
- One-way sync: emails which are moved or deleted will not be moved locally.

Most limitations of `dovemail` can be worked around by opening the email in a web browser by using the `X-Web-Link` header.
