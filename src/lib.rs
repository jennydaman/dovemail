pub mod outlook;
mod write;

pub use write::{write_message, MessageWriteError};

#[cfg(test)]
mod test {
    use super::outlook::*;
    use super::*;
    use rstest::*;
    use std::fs::File;
    use std::io::BufReader;
    use std::path::{Path, PathBuf};
    use time::format_description::well_known::Rfc2822;

    #[rstest]
    fn test_write_message(example_message: OutlookMessage) {
        let mut buf: Vec<u8> = Vec::new();
        write_message(&example_message, &mut buf).unwrap();
        let actual = String::from_utf8(buf).unwrap();
        let OutlookMessage {
            internet_message_id,
            sent_date_time,
            from,
            web_link,
            body,
            ..
        } = example_message;
        let expected_lines = [
            format!("Message-ID: {}\n", internet_message_id),
            format!("Date: {}\n", sent_date_time.format(&Rfc2822).unwrap()),
            format!("From: {}\n", from.unwrap()),
            format!("X-Web-Link: {}\n", web_link),
        ];
        for expected_line in expected_lines {
            assert!(actual.contains(&expected_line));
        }
        assert!(actual.ends_with(&format!("\n\n{}\n", body.content)))
    }

    #[fixture]
    fn example_message(examples_dir: PathBuf) -> OutlookMessage {
        let file = File::open(examples_dir.join("message.json")).unwrap();
        let reader = BufReader::new(file);
        serde_json::from_reader(reader).unwrap()
    }

    #[fixture]
    fn examples_dir() -> PathBuf {
        Path::new(env!("CARGO_MANIFEST_DIR")).join("testdata")
    }
}
